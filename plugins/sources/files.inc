<?php

/**
 * @file
 * Support for migration from files sources.
 */

/**
 * Implementation of MigrateList, for retrieving a list of IDs to be migrated
 * from a directory listing. Each item is a file, it's ID is the path.
 */
class MigrateListFiles extends MigrateList {

  /**
   * A URI pointing to a path to begin scanning.
   *
   * @var string
   */
  protected $listDirs;
  protected $baseDir;
  protected $fileMask;
  protected $directoryOptions;

  public function __construct($list_dirs, $base_dir, $file_mask = NULL, $options = array()) {
    parent::__construct();
    $this->listDirs = $list_dirs;
    $this->baseDir = $base_dir;
    $this->fileMask = $file_mask;
    $this->directoryOptions = $options;
  }

  /**
   * Our public face is the URI we're getting items from
   *
   * @return string
   */
  public function __toString() {
    if (is_array($this->listDirs)) {
      return implode(',', $this->listDirs);
    }
    else {
      return $this->listDirs;
    }
  }

  /**
   * Retrieve a list of files based on parameters passed for the migration.
   *
   * @return array
   */
  public function getIdList() {
    $files = array();
    foreach ($this->listDirs as $dir) {
      migrate_instrument_start("Retrieve $dir");
      $files = array_merge(file_scan_directory($dir, $this->fileMask, $this->directoryOptions), $files);
      migrate_instrument_stop("Retrieve $dir");
    }

    if (isset($files)) {
      return $this->getIDsFromFiles($files);
    }
    $migration = Migration::currentMigration();
    $migration->showMessage(t('Loading of !listuri failed:', array('!listuri' => $this->listUri)));
    return NULL;
  }

  /**
   * Given an array generated from file_scan_directory(), parse out the IDs for processing
   * and return them as an array.
   *
   * @param array $files
   *
   * @return array
   */
  protected function getIDsFromFiles(array $files) {
    $ids = array();
    foreach ($files as $file) {
      $ids[] = str_replace($this->baseDir, '', (string) $file->uri);
    }
    return array_unique($ids);
  }

  /**
   * Return a count of all available IDs from the source listing.
   */
  public function computeCount() {
    $count = 0;
    $files = $this->getIdList();
    if ($files) {
      $count = count($files);
    }
    return $count;
  }

}

/**
 * Implementation of MigrateItem, for retrieving a file from the file system
 * based on source directory and an ID provided by a MigrateList class.
 */
class MigrateItemFile extends MigrateItem {

  protected $baseDir;

  public function __construct($base_dir) {
    parent::__construct();
    $this->baseDir = $base_dir;
  }

  /**
   * Implementors are expected to return an object representing a source item.
   *
   * @param mixed $id
   *
   * @return stdClass
   */
  public function getItem($id) {
    $item_uri = $this->baseDir . $id;
    // Get the file data at the specified URI
    $data = $this->loadFile($item_uri);
    if ($data) {
      $return = new stdClass;
      $return->filedata = $data;
      return $return;
    }
    else {
      $migration = Migration::currentMigration();
      $message = t('Loading of !objecturi failed:', array('!objecturi' => $item_uri));
      $migration->getMap()->saveMessage(
              array($id), $message, MigrationBase::MESSAGE_ERROR);
      return NULL;
    }
  }

  /**
   * Default File loader.
   */
  protected function loadFile($item_uri) {
    $data = file_get_contents($item_uri);
    return $data;
  }

}